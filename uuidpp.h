#ifndef CTP_UUIDPP_H
#define CTP_UUIDPP_H

#include <chrono>
#include <cstddef>
#include <initializer_list>
#include <stdexcept>
#include <string>
#include <utility>
extern "C"
{
#include <uuid/uuid.h>
}

namespace ctp
{
  class uuid;

  inline namespace literals
  {
    inline namespace string_literals
    {
      uuid operator ""_uuid(const char *, std::size_t);
    }
  }

  class uuid
  {
  public:
    enum class variant
    {
      ncs = UUID_VARIANT_NCS,
      dce = UUID_VARIANT_DCE,
      microsoft = UUID_VARIANT_MICROSOFT,
      other = UUID_VARIANT_OTHER
    };

    enum class type
    {
      dce_time = UUID_TYPE_DCE_TIME,
      dce_random = UUID_TYPE_DCE_RANDOM
    };

    class parse_error: public std::runtime_error { public: parse_error(); };
    class safety_error: public std::runtime_error { public: safety_error(); };

    uuid() noexcept = default;

    uuid(const uuid &) noexcept;
    uuid &operator=(const uuid &) noexcept;

    bool is_null() const noexcept;
    explicit operator bool() const noexcept;

    static uuid null() noexcept;
    static uuid generate() noexcept;
    static uuid generate_random() noexcept;
    static uuid generate_time() noexcept;
    static std::pair<uuid, bool> generate_time_safe() noexcept;
    static uuid generate_time_safe_throw();

    static uuid parse(const std::string &);
    std::string unparse() const;
    std::string unparse_lower() const;
    std::string unparse_upper() const;

    std::chrono::system_clock::time_point time() const noexcept;

    enum type type() const noexcept;
    enum variant variant() const noexcept;

    friend int compare(const uuid &, const uuid &) noexcept;
    friend uuid literals::string_literals::operator ""_uuid(const char *, std::size_t);

  private:
    ::uuid_t m_uuid;
  };

  bool operator==(const uuid &, const uuid &) noexcept;
  bool operator!=(const uuid &, const uuid &) noexcept;
  bool operator<(const uuid &, const uuid &) noexcept;
  bool operator<=(const uuid &, const uuid &) noexcept;
  bool operator>(const uuid &, const uuid &) noexcept;
  bool operator>=(const uuid &, const uuid &) noexcept;
}


// PRIVATE

extern "C"
{
#include <sys/time.h>
}

namespace ctp
{
  inline uuid literals::string_literals::operator ""_uuid(const char *const s, std::size_t)
  {
    uuid ret;
    if (::uuid_parse(s, ret.m_uuid) != 0)
      throw uuid::parse_error();
    return ret;
  }

  inline uuid::parse_error::parse_error(): runtime_error("uuid::parse") { }
  inline uuid::safety_error::safety_error(): runtime_error("uuid::generate_time_safe_throw") { }

  inline uuid::uuid(const uuid &other) noexcept { ::uuid_copy(m_uuid, other.m_uuid); }
  inline uuid &uuid::operator=(const uuid &other) noexcept
  { ::uuid_copy(m_uuid, other.m_uuid); return *this; }

  inline bool uuid::is_null() const noexcept { return static_cast<bool>(::uuid_is_null(m_uuid)); }
  inline uuid::operator bool() const noexcept { return !is_null(); }

  inline uuid uuid::null() noexcept { uuid ret; ::uuid_clear(ret.m_uuid); return ret; }
  inline uuid uuid::generate() noexcept { uuid ret; ::uuid_generate(ret.m_uuid); return ret; }
  inline uuid uuid::generate_random() noexcept { uuid ret; ::uuid_generate_random(ret.m_uuid); return ret; }
  inline uuid uuid::generate_time() noexcept { uuid ret; ::uuid_generate_time(ret.m_uuid); return ret; }
  inline std::pair<uuid, bool> uuid::generate_time_safe() noexcept
  {
    std::pair<uuid, bool> ret;
    ret.second = ::uuid_generate_time_safe(ret.first.m_uuid) == 0;
    return ret;
  }
  inline uuid uuid::generate_time_safe_throw()
  {
    uuid ret;
    if (::uuid_generate_time_safe(ret.m_uuid) != 0)
      throw safety_error();
    return ret;
  }

  inline uuid uuid::parse(const std::string &in)
  {
    uuid ret;
    if (::uuid_parse(in.c_str(), ret.m_uuid) != 0)
      throw parse_error();
    return ret;
  }

  inline std::string uuid::unparse() const
  {
    char ret[37];
    ::uuid_unparse(m_uuid, ret);
    return std::string(ret);
  }

  inline std::string uuid::unparse_lower() const
  {
    char ret[37];
    ::uuid_unparse_lower(m_uuid, ret);
    return std::string(ret);
  }

  inline std::string uuid::unparse_upper() const
  {
    char ret[37];
    ::uuid_unparse_upper(m_uuid, ret);
    return std::string(ret);
  }

  inline std::chrono::system_clock::time_point uuid::time() const noexcept
  {
    struct timeval tv;
    ::uuid_time(m_uuid, &tv);
    return std::chrono::system_clock::from_time_t(tv.tv_sec) +
      std::chrono::microseconds(tv.tv_usec);
  }

  inline enum uuid::type uuid::type() const noexcept { return static_cast<enum type>(::uuid_type(m_uuid)); }
  inline enum uuid::variant uuid::variant() const noexcept { return static_cast<enum variant>(::uuid_variant(m_uuid)); }

  inline int compare(const uuid &lhs, const uuid &rhs) noexcept
  { return ::uuid_compare(lhs.m_uuid, rhs.m_uuid); }

  inline bool operator==(const uuid &lhs, const uuid &rhs) noexcept { return compare(lhs, rhs) == 0; }
  inline bool operator!=(const uuid &lhs, const uuid &rhs) noexcept { return compare(lhs, rhs) != 0; }
  inline bool operator<(const uuid &lhs, const uuid &rhs) noexcept { return compare(lhs, rhs) < 0; }
  inline bool operator<=(const uuid &lhs, const uuid &rhs) noexcept { return compare(lhs, rhs) <= 0; }
  inline bool operator>(const uuid &lhs, const uuid &rhs) noexcept { return compare(lhs, rhs) > 0; }
  inline bool operator>=(const uuid &lhs, const uuid &rhs) noexcept { return compare(lhs, rhs) >= 0; }
}

#endif
